from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """
    The User model is someone that logs in to administer the
    conference application, not someone that is attending or
    presenting at the conference.

    The custom user model for this project as advised by Django docs
    https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
    """


# @require_http_methods(["GET", "POST"])
# def api_list_locations(request):
#     if request.method == "GET":
#         locations = Location.objects.all()
#         return JsonResponse(
#             {"locations": locations},
#             encoder=LocationListEncoder)
#     else:
#         content = json.loads(request.body)
#     try:
#         state = State.objects.get(abbreviation=content["state"])
#         content["state"] = state
#     except State.DoesNotExist:
#         return JsonResponse({"message": "Invalid state abbreviation"}, status=400)

# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_location(request, pk):
#     if request.method == "GET":
#         location = Location.objects.get(id=pk)
#         return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
#     elif request.method == "DELETE":
#         count, _ = Location.objects.filter(id=pk).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         content = json.loads(request.body)
#         try:
#             if "state" in content:
#                 state = State.objects.get(abbreviation=content["state"])
#                 content["state"] = state
#         except State.DoesNotExist:
#             return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
#         Location.objects.filter(id=pk).update(**content)
#         location = Location.objects.get(id=pk)
#         return JsonResponse(
#             location, encoder=LocationDetailEncoder, safe=False)